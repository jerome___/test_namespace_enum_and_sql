<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>DialogConnectDatabase</name>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.ui" line="14"/>
        <source>Connect to a database</source>
        <oldsource>Se connecter à une base de donnée</oldsource>
        <translation>Se connecter à la base de données</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.ui" line="28"/>
        <source>Choose database to be connected with</source>
        <oldsource>Choisir une base de donnée à laquelle se connecter</oldsource>
        <translation>Choisir une base de donnée à laquelle se connecter</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.ui" line="40"/>
        <location filename="../Dialog/dialogConnectDatabase.cpp" line="87"/>
        <source>Connect</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.ui" line="50"/>
        <source>Change User</source>
        <translation>Changez d&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.ui" line="70"/>
        <source>Create database with usable table</source>
        <translation>Créez une base utilisable</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.ui" line="77"/>
        <source>Delete selected database</source>
        <translation>Supprimer la base de donnée</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.ui" line="97"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Annuler</translation>
    </message>
    <message>
        <source>Connecter</source>
        <translation type="vanished">Se connecter</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <source>Déconnecter</source>
        <translation type="vanished">Se déconnecter</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.cpp" line="47"/>
        <source>Define a user to be able to connect.</source>
        <translation>Définissez un utilisateur qui pourra se connecter</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.cpp" line="59"/>
        <source>Remove database</source>
        <translation>Confirmation de suppression de la base de donnée</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.cpp" line="60"/>
        <source>Are you sure to remove this database ?</source>
        <translation>Êtes vous sûr de vraiment vouloir suprimer la base de donnée ?</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.cpp" line="69"/>
        <source>Impossible</source>
        <translation>Impossible !</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.cpp" line="70"/>
        <source>You can not delete this database,
because she has not a usable table &quot;numbers&quot;</source>
        <translation>Vous ne pouvez pas supprimer cette base de donnée, parce que celle-ci ne contient pas de table utilisable et n&apos;appartient donc pas au programme.</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.cpp" line="86"/>
        <source>Disconnect</source>
        <translation>Se déconnecter</translation>
    </message>
</context>
<context>
    <name>DialogConnectServer</name>
    <message>
        <location filename="../Dialog/dialogConnectServer.ui" line="14"/>
        <source>Connect to Postgresql server</source>
        <oldsource>Se connecter à un server Postgresql</oldsource>
        <translation>Connexion au server Postgresql</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.ui" line="28"/>
        <source>Connect to postgresql server</source>
        <oldsource>Se connecter au serveur Postgresql</oldsource>
        <translation>Coordonnées de connexion</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.ui" line="38"/>
        <source>Host name</source>
        <translation>Nom de machine</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.ui" line="45"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Can be an adress with ipv4 or ipv6 or a host name&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>Peut être une adresse ipv4 ou ipv6 ou un nom d&apos;hôte</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.ui" line="52"/>
        <source>User name</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.ui" line="83"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;default port for postgresql is 5432, but your server can be any else other.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>le port par défaut pour Postgresql est 5432, mais il peut être différent.</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.ui" line="114"/>
        <location filename="../Dialog/dialogConnectServer.cpp" line="72"/>
        <source>Connect</source>
        <translation>Se connecter</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.ui" line="137"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.ui" line="62"/>
        <source>Password</source>
        <oldsource>Mot de passe</oldsource>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.ui" line="76"/>
        <source>Server port</source>
        <oldsource>Port du serveur</oldsource>
        <translation>Port d&apos;accès</translation>
    </message>
    <message>
        <source>Connecter</source>
        <translation type="vanished">Se connecter</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.ui" line="144"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Se connecter en tant qu&apos;administrateur au serveur</source>
        <translation type="vanished">Se connecter en tant qu&apos;administrateur au server</translation>
    </message>
    <message>
        <source>Les coordonnées pour se connecter à la base de donnée</source>
        <translation type="vanished">Coordonnée de connexion à la base</translation>
    </message>
    <message>
        <source>Déconnecter</source>
        <translation type="vanished">Se déconnecter</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.cpp" line="11"/>
        <source>Connect to server as Administrator</source>
        <translation>Se connecter au serveur en tant qu&apos;administrateur</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.cpp" line="15"/>
        <source>Database connection coordonates</source>
        <translation>Coordonnée de connexion à la base de donnée</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.cpp" line="71"/>
        <source>Disconnect</source>
        <translation>Se déconnecter</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.cpp" line="79"/>
        <source>You forget to indicate a host name.</source>
        <translation>Vus avez oublié d&apos;indiquer le nom (ou l&apos;adresse) de la machine.</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.cpp" line="83"/>
        <source>You forget to indicate the user name.</source>
        <translation>Vous avez oublié le nom de l&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.cpp" line="99"/>
        <source>Connexion refused due to missing password.</source>
        <translation>Connexion refusée. Il manque le mot de passe.</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.cpp" line="106"/>
        <source>Connexion refused due to wrong password.</source>
        <translation>Connexion refusée. Mot de passe erroné.</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.cpp" line="113"/>
        <source>Connexion refused due to unknow host name.</source>
        <translation>Connexion refusée. Nom de la machine inconnu.</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.cpp" line="120"/>
        <source>Connexion refused due to non active host adress/port.</source>
        <translation>Connexion refusée. Adresse ou port inactif (vérifiez l&apos;un et/ou l&apos;autre)</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.cpp" line="133"/>
        <source>You should give minimal informations</source>
        <translation>Vous devez indiquer un minimum d&apos;informations.</translation>
    </message>
</context>
<context>
    <name>DialogCreateDatabase</name>
    <message>
        <location filename="../Dialog/dialogCreateDatabase.ui" line="26"/>
        <source>Create a database with default model</source>
        <translation>Créer une base de donnée avec le modèle par défaut</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogCreateDatabase.ui" line="40"/>
        <source>Give a name to this new database to create</source>
        <translation>Nom et description de la nouvelle base</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogCreateDatabase.ui" line="62"/>
        <source>Database name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogCreateDatabase.ui" line="72"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogCreateDatabase.ui" line="112"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogCreateDatabase.ui" line="119"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogCreateDatabase.cpp" line="28"/>
        <source>Vous oubliez quelque chose</source>
        <translation>Vous oubliez quelque chose</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogCreateDatabase.cpp" line="29"/>
        <source>Indiquez le nom de la base de donnée à créer.</source>
        <translation>Vous devez indiquer le nom de la base de données à créer</translation>
    </message>
</context>
<context>
    <name>DialogNewData</name>
    <message>
        <location filename="../Dialog/dialogNewData.ui" line="14"/>
        <source>Add data to the table</source>
        <translation>Ajouter des données à la table</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogNewData.ui" line="29"/>
        <source>Add new data to the table</source>
        <translation>Données à ajouter</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogNewData.ui" line="51"/>
        <source>Name</source>
        <oldsource>Nom</oldsource>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogNewData.ui" line="61"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogNewData.ui" line="71"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogNewData.ui" line="111"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogNewData.ui" line="118"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <source>Enum type test for database with Postgresql and Qt-5</source>
        <oldsource>Test de ENUM dans Postgresql et Qt-5</oldsource>
        <translation>Test de type Enum avec Postgresql</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="31"/>
        <source>New Data</source>
        <translation>Ajouter une ligne de donnée</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="38"/>
        <source>Remove selection</source>
        <translation>Supprimez la sélection</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="62"/>
        <source>Action</source>
        <translation>Action</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="85"/>
        <source>&amp;Manage databases</source>
        <translation>Superviser les bases de données</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="95"/>
        <source>&amp;Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <source>Quit</source>
        <oldsource>Quiter</oldsource>
        <translation type="vanished">Quitter</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="55"/>
        <source>Post&amp;gresql Server</source>
        <oldsource>Serveur Post&amp;gresql</oldsource>
        <translation>Serveur Postgresql</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="80"/>
        <source>&amp;Server connection</source>
        <oldsource>&amp;Connexion au Serveur</oldsource>
        <translation>Connexion au serveur</translation>
    </message>
    <message>
        <source>&amp;Database connection</source>
        <translation type="vanished">Connexion à la base</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="90"/>
        <source>&amp;Create database</source>
        <translation>Créer une base de données</translation>
    </message>
    <message>
        <source>Aucune connexion active</source>
        <translation type="vanished">Pas de connexion</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="21"/>
        <source>No database connected</source>
        <translation>Pas de base de donnée connectée</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="22"/>
        <source>No active connection</source>
        <translation>Pas de connexion active</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="58"/>
        <source>You need to connect on a database 
who has a usable table.</source>
        <translation>Vous devez vous connecter à une base de donnée qui contient une table au format souhaité.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="93"/>
        <source>Ok, this databse has a usable table.</source>
        <translation>Ok, cette base de donnée contient une table exploitable.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="99"/>
        <source>For this database, there is no correct table.
You need to choose an other one one database
or create a new one.</source>
        <translation>Pour cette base de donnée, aucune table exploitable n&apos;existe.</translation>
    </message>
    <message>
        <source>Define a user to be able to connect.</source>
        <translation type="vanished">Définissez un utilisateur qui pourra se connecter</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="198"/>
        <source>Adiminstrator connected to server </source>
        <translation>Connexion de l&apos;administrateur au serveur</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="201"/>
        <source>No administrator connection </source>
        <translation>Pas d&apos;administrateur connecté</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="205"/>
        <source>Connected to databse %1</source>
        <translation>Connecté à la base de donnée %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="208"/>
        <source>No connection to database.</source>
        <translation>Pas de base de donnée connectée</translation>
    </message>
    <message>
        <source>Définissez un utilisateur qui se connectera.</source>
        <translation type="vanished">Quel utilisateur se connectera ? Vous devez le définir.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="158"/>
        <source>Nom</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="159"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="160"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <source>Adiminstrateur connecté au serveur Postgresql </source>
        <translation type="vanished">Administrateur connecté au serveur</translation>
    </message>
    <message>
        <source>Aucune connexion d&apos;administration au serveur </source>
        <translation type="vanished">Pas d&apos;administrateur connecté</translation>
    </message>
    <message>
        <source>| connecté à la base de donnée %1</source>
        <translation type="vanished">Connecté à la base </translation>
    </message>
    <message>
        <source>| pas de connexion à la base de données.</source>
        <translation type="vanished">Pas de connexion à la base</translation>
    </message>
</context>
</TS>
