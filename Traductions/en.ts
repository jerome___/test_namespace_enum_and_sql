<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>DialogConnectDatabase</name>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.ui" line="14"/>
        <source>Connect to a database</source>
        <oldsource>Se connecter à une base de donnée</oldsource>
        <translation>Connect to a database</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.ui" line="28"/>
        <source>Choose database to be connected with</source>
        <oldsource>Choisir une base de donnée à laquelle se connecter</oldsource>
        <translation>Choose a database to be connected with</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.ui" line="40"/>
        <location filename="../Dialog/dialogConnectDatabase.cpp" line="87"/>
        <source>Connect</source>
        <translation>Connect</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.ui" line="50"/>
        <source>Change User</source>
        <translation>Change user</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.ui" line="70"/>
        <source>Create database with usable table</source>
        <translation>Create usable database</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.ui" line="77"/>
        <source>Delete selected database</source>
        <translation>Delete database</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.ui" line="97"/>
        <source>Close</source>
        <translation>Close</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Cancel</translation>
    </message>
    <message>
        <source>Connecter</source>
        <translation type="vanished">Connect</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <source>Déconnecter</source>
        <translation type="vanished">Disconnect</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.cpp" line="47"/>
        <source>Define a user to be able to connect.</source>
        <translation>Define a user to be able to connect</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.cpp" line="59"/>
        <source>Remove database</source>
        <translation>Confirm to remove database</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.cpp" line="60"/>
        <source>Are you sure to remove this database ?</source>
        <translation>Do you really want to delete this database ?</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.cpp" line="69"/>
        <source>Impossible</source>
        <translation>Impossible !</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.cpp" line="70"/>
        <source>You can not delete this database,
because she has not a usable table &quot;numbers&quot;</source>
        <translation>You can not delete this database, because there is no usable table inside and it is probably not a database for this software.</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectDatabase.cpp" line="86"/>
        <source>Disconnect</source>
        <translation>Disconnect</translation>
    </message>
</context>
<context>
    <name>DialogConnectServer</name>
    <message>
        <location filename="../Dialog/dialogConnectServer.ui" line="14"/>
        <source>Connect to Postgresql server</source>
        <oldsource>Se connecter à un server Postgresql</oldsource>
        <translation>Connect to Postgresql database</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.ui" line="28"/>
        <source>Connect to postgresql server</source>
        <oldsource>Se connecter au serveur Postgresql</oldsource>
        <translation>Connect to Postgresql server</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.ui" line="38"/>
        <source>Host name</source>
        <translation>Host name</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.ui" line="45"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Can be an adress with ipv4 or ipv6 or a host name&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>Can be an adress with ipv4 or ipv6 or a host name</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.ui" line="52"/>
        <source>User name</source>
        <translation>User name</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.ui" line="83"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;default port for postgresql is 5432, but your server can be any else other.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>Default port for postgresql is 5432, but your server can be any else other.</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.ui" line="114"/>
        <location filename="../Dialog/dialogConnectServer.cpp" line="72"/>
        <source>Connect</source>
        <translation>Connect</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.ui" line="137"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.ui" line="62"/>
        <source>Password</source>
        <oldsource>Mot de passe</oldsource>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.ui" line="76"/>
        <source>Server port</source>
        <oldsource>Port du serveur</oldsource>
        <translation>Server port</translation>
    </message>
    <message>
        <source>Connecter</source>
        <translation type="vanished">Connect</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.ui" line="144"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Se connecter en tant qu&apos;administrateur au serveur</source>
        <translation type="vanished">Connect to server as admin user</translation>
    </message>
    <message>
        <source>Les coordonnées pour se connecter à la base de donnée</source>
        <translation type="vanished">Identifier datas for connection</translation>
    </message>
    <message>
        <source>Déconnecter</source>
        <translation type="vanished">Disconnect</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.cpp" line="11"/>
        <source>Connect to server as Administrator</source>
        <translation>Connect to server as administrator</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.cpp" line="15"/>
        <source>Database connection coordonates</source>
        <translation>Database connection coordonates</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.cpp" line="71"/>
        <source>Disconnect</source>
        <translation>Disconnect</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.cpp" line="79"/>
        <source>You forget to indicate a host name.</source>
        <translation>You forget to indicate a host name.</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.cpp" line="83"/>
        <source>You forget to indicate the user name.</source>
        <translation>You forget to indicate the user name</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.cpp" line="99"/>
        <source>Connexion refused due to missing password.</source>
        <translation>Connection refused due to missing password.</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.cpp" line="106"/>
        <source>Connexion refused due to wrong password.</source>
        <translation>Connection refused due to wrong password.</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.cpp" line="113"/>
        <source>Connexion refused due to unknow host name.</source>
        <translation>Connection refused due to unknow host name.</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.cpp" line="120"/>
        <source>Connexion refused due to non active host adress/port.</source>
        <translation>Connection refused due to non active host adress/port (check adress and/or port).</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogConnectServer.cpp" line="133"/>
        <source>You should give minimal informations</source>
        <translation>You should provide minimal informations</translation>
    </message>
</context>
<context>
    <name>DialogCreateDatabase</name>
    <message>
        <location filename="../Dialog/dialogCreateDatabase.ui" line="26"/>
        <source>Create a database with default model</source>
        <translation>Create a database with default model</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogCreateDatabase.ui" line="40"/>
        <source>Give a name to this new database to create</source>
        <translation>New database name and description</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogCreateDatabase.ui" line="62"/>
        <source>Database name</source>
        <translation>Database name</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogCreateDatabase.ui" line="72"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogCreateDatabase.ui" line="112"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogCreateDatabase.ui" line="119"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogCreateDatabase.cpp" line="28"/>
        <source>Vous oubliez quelque chose</source>
        <translation>You missed something</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogCreateDatabase.cpp" line="29"/>
        <source>Indiquez le nom de la base de donnée à créer.</source>
        <translation>You have to give a name to the new database</translation>
    </message>
</context>
<context>
    <name>DialogNewData</name>
    <message>
        <location filename="../Dialog/dialogNewData.ui" line="14"/>
        <source>Add data to the table</source>
        <translation>New row data to add in the table</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogNewData.ui" line="29"/>
        <source>Add new data to the table</source>
        <translation>Informations of new data row</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogNewData.ui" line="51"/>
        <source>Name</source>
        <oldsource>Nom</oldsource>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogNewData.ui" line="61"/>
        <source>Type</source>
        <translation>Typr</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogNewData.ui" line="71"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogNewData.ui" line="111"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../Dialog/dialogNewData.ui" line="118"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="14"/>
        <source>Enum type test for database with Postgresql and Qt-5</source>
        <oldsource>Test de ENUM dans Postgresql et Qt-5</oldsource>
        <translation>Enum type test with Postgresql</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="31"/>
        <source>New Data</source>
        <translation>New Data</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="38"/>
        <source>Remove selection</source>
        <translation>Remove the selection</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="62"/>
        <source>Action</source>
        <translatorcomment>Action</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="85"/>
        <source>&amp;Manage databases</source>
        <translation>Manage Databases</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="95"/>
        <source>&amp;Quit</source>
        <translation>Quit</translation>
    </message>
    <message>
        <source>Quit</source>
        <oldsource>Quiter</oldsource>
        <translation type="vanished">Quit</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="55"/>
        <source>Post&amp;gresql Server</source>
        <oldsource>Serveur Post&amp;gresql</oldsource>
        <translation>Postgresql</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="80"/>
        <source>&amp;Server connection</source>
        <oldsource>&amp;Connexion au Serveur</oldsource>
        <translation>Server connection</translation>
    </message>
    <message>
        <source>&amp;Database connection</source>
        <translation type="vanished">Database connection</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="90"/>
        <source>&amp;Create database</source>
        <translation>Create database</translation>
    </message>
    <message>
        <source>Aucune connexion active</source>
        <translation type="vanished">No active connection</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="21"/>
        <source>No database connected</source>
        <translation>No databse connected</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="22"/>
        <source>No active connection</source>
        <translation>No active connection</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="58"/>
        <source>You need to connect on a database 
who has a usable table.</source>
        <translation>You need to connect on a database who has a usable table</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="93"/>
        <source>Ok, this databse has a usable table.</source>
        <translation>This database has a usable table. Good.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="99"/>
        <source>For this database, there is no correct table.
You need to choose an other one one database
or create a new one.</source>
        <translation>There is no correct table inside this database. You need to choose an other database.</translation>
    </message>
    <message>
        <source>Define a user to be able to connect.</source>
        <translation type="vanished">Define a user to be able to connect</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="198"/>
        <source>Adiminstrator connected to server </source>
        <translation>Administrator connected to server</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="201"/>
        <source>No administrator connection </source>
        <translation>No administrator connection</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="205"/>
        <source>Connected to databse %1</source>
        <translation>Connected on database %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="208"/>
        <source>No connection to database.</source>
        <translation>No database connection</translation>
    </message>
    <message>
        <source>Définissez un utilisateur qui se connectera.</source>
        <translation type="vanished">Define user to connect</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="158"/>
        <source>Nom</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="159"/>
        <source>Type</source>
        <translation>Tpe</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="160"/>
        <source>Code</source>
        <translation>Code</translation>
    </message>
    <message>
        <source>Adiminstrateur connecté au serveur Postgresql </source>
        <translation type="vanished">Admin is connected to server</translation>
    </message>
    <message>
        <source>Aucune connexion d&apos;administration au serveur </source>
        <translation type="vanished">no admin connection to server</translation>
    </message>
    <message>
        <source>| connecté à la base de donnée %1</source>
        <translation type="vanished">connected to database </translation>
    </message>
    <message>
        <source>| pas de connexion à la base de données.</source>
        <translation type="vanished">No database connection</translation>
    </message>
</context>
</TS>
