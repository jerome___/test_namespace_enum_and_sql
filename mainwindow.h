#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLineEdit>
#include <QTableView>
#include <QPushButton>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QHeaderView>
#include <QLabel>
#include <QHBoxLayout>
#include <QModelIndex>
#include <QMap>
#include "Database/PostgresDB.h"
#include "Database/NamesDB.h"
#include "Dialog/dialogConnectServer.h"
#include "Dialog/dialogConnectDatabase.h"
#include "Dialog/dialogCreateDatabase.h"
#include "Dialog/dialogNewData.h"
#include "ui_mainwindow.h"
#include "Delegators/typeEditComboBox.h"

namespace Ui { class MainWindow; }

class MainWindow : public QMainWindow {
	    Q_OBJECT
	enum class eventOrigin { POSTGRES, NAMES };
public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private slots:
	void on_actionConnect_Server_triggered();
	void on_actionConnect_Database_triggered();
	void on_actionQuit_triggered();
	void on_serverConnection();
	void on_databaseConnection();
	void on_newData_clicked();
	void on_removeData_clicked();
	void on_table_clicked();
	void on_itemTableChanged(QStandardItem *item);

signals:
	void addData(const QString &n, Database::Media t, const QString &c );
	void host(const QString &s);
	void dbName(const QString s);
	void user(const QString &s);
	void port(const int &i);
	void password(const QString &s);
	void newDatabaseCreated();

private:
	Ui::MainWindow			*ui;
	Database::PostgresDB	*_postgresql;
	Database::NamesDB		*_Names;
	QStandardItemModel		*tableModel;
	TypeEditComboBox		*itemTypeDelegate;
	QPalette				colorMessage;
	QWidget					*statusW;
	QHBoxLayout				*hStatusBox;
	QLabel					*statusServer, *statusDB, *separator;
	DialogConnectServer		*serverPostgresConnector, *serverNamesConnector;
	DialogConnectDatabase	*dbConnector;
	QString					selectedName;
	QString					oldName, noConnectionDatabase;
	void	populateTable();
	void	defStatusBar();
	bool	checkIfContainTable();
};
#endif // MAINWINDOW_H
