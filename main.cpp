#include "mainwindow.h"
#include <QApplication>
#include <QTranslator>
#include <QLibraryInfo>
#include <QDebug>

int main(int argc, char *argv[]) {
	QString langue = QLocale::system().name().split("_")[0];
	QTranslator traducteur;
	traducteur.load(QString(":/i18n/%1").arg(langue));
	QApplication a(argc, argv);
	qApp->installTranslator(&traducteur);
	MainWindow w;
	w.show();
	return a.exec();
}
