#ifndef DIALOGCREATEDATABASE_H
#define DIALOGCREATEDATABASE_H

#include <QDialog>
#include <QMessageBox>
#include <QLineEdit>
#include <QTextEdit>
#include "Database/PostgresDB.h"
#include "Database/NamesDB.h"
#include "Dialog/dialogConnectServer.h"

namespace Ui { class DialogCreateDatabase; }

class DialogCreateDatabase : public QDialog {
	Q_OBJECT

public:
	explicit DialogCreateDatabase(Database::PostgresDB *d,
	                              Database::NamesDB *n,
	                              QWidget *parent = 0);
	~DialogCreateDatabase();

private slots:
	void on_Cancel_clicked();
	void on_OK_clicked();

private:
	Ui::DialogCreateDatabase	*ui;
	Database::PostgresDB		*postgres;
	Database::NamesDB			*names;
};

#endif // DIALOGCREATEDATABASE_H
