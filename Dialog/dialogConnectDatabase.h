#ifndef DIALOGCONNECTDATABASE_H
#define DIALOGCONNECTDATABASE_H

#include <QDialog>
#include <QStandardItemModel>
#include <QTableView>
#include <QPushButton>
#include "Database/NamesDB.h"
#include "Database/PostgresDB.h"
#include "dialogConnectServer.h"
#include "dialogCreateDatabase.h"

namespace Ui { 	class DialogConnectDatabase; }

class DialogConnectDatabase : public QDialog {
	Q_OBJECT

public:
	explicit DialogConnectDatabase(Database::PostgresDB *s,
	                               Database::NamesDB *n,
	                               QWidget *parent = 0);
	~DialogConnectDatabase();

signals:
	void DatabaseConnectionStateEvent();

private slots:
	void on_connect_clicked();
	void on_changeUser_clicked();
	void on_createDB_clicked();
	void on_deleteDB_clicked();
	void on_Close_clicked();
	void on_databases_clicked();
	void on_serverConnected();
	void on_databaseCheckConnection();
	void on_newDBcreated();

private:
	Ui::DialogConnectDatabase	*ui;
	DialogConnectServer			*connectNamesServer;
	DialogCreateDatabase		*creator;
	Database::NamesDB			*names;
	Database::PostgresDB		*server;
	QStandardItemModel			*databasesModel;
	QString						selectedDBname;
	int							rejected;
	void populateDatabases();
};

#endif // DIALOGCONNECTDATABASE_H
