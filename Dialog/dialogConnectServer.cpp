#include "dialogConnectServer.h"
#include "ui_dialogConnectServer.h"
#include "Database/PostgresDB.h"
#include "Database/NamesDB.h"
#include <QMessageBox>

DialogConnectServer::DialogConnectServer(Database::DB *db, QWidget *parent, const QString &host) :
    QDialog(parent), _db(db), _host(host), ui(new Ui::DialogConnectServer) {
	ui->setupUi(this);
	switch (_db->whoAmI()) {
		case Database::Type::Postgres :
			ui->title->setText(tr("Connect to server as Administrator"));
			ui->connect->show();
			break;
		case Database::Type::Names :
			ui->title->setText(tr("Database connection coordonates"));
			ui->connect->hide();
			if (!_host.isEmpty()) {
				Database::NamesDB *db = dynamic_cast<Database::NamesDB*>(_db);
				db->setHostName(_host);
			}
			break;
	}
	populateFields();
	connect(ui->hostname, SIGNAL(textChanged(QString)), _db, SLOT(on_host(QString)));
	connect(ui->username, SIGNAL(textChanged(QString)), _db, SLOT(on_user(QString)));
	connect(ui->password, SIGNAL(textChanged(QString)), _db, SLOT(on_password(QString)));
	connect(ui->port,	  SIGNAL(valueChanged(int)),		  _db, SLOT(on_port(int)));
	connect(this, SIGNAL(ServerConnection()), this, SLOT(on_serverConnection()));
	ui->password->setEchoMode(QLineEdit::Password);
}

DialogConnectServer::~DialogConnectServer() {
	delete ui;
}

void DialogConnectServer::on_connect_clicked() {
	if (!checkValidity()) {
		printWarnings();
	} else {
		(ui->connect->isChecked()) ? _db->connectDB() : _db->disConnectDB();
		emit ServerConnection();
	}
	ui->connect->setChecked(_db->isConnected());
}

void DialogConnectServer::on_Cancel_clicked() {
	reject();
}

void DialogConnectServer::on_OK_clicked() {
	if (!checkValidity()) {
		printWarnings();
	} else {
		switch (_db->whoAmI()) {
			case Database::Type::Names :
				break;
			case Database::Type::Postgres :
				_db->connectDB();
				break;
		}
		emit ServerConnection();
		accept();
	}
}

void DialogConnectServer::populateFields() {
	ui->hostname->setText(_db->getHostname());
	ui->username->setText(_db->getUsername());
	ui->password->setText(_db->getPassword());
	ui->port->setValue(_db->getPort());
	ui->connect->setChecked(_db->isConnected());
}

void DialogConnectServer::on_serverConnection() {
	ui->connect->setChecked(_db->isConnected());
	(_db->isConnected()) ? ui->connect->setText(tr("Disconnect"))
	                     : ui->connect->setText(tr("Connect"));
}

bool DialogConnectServer::checkValidity() {
	errors.clear();
	QString error("");
	if (ui->hostname->text().isEmpty()) {
		error = tr("You forget to indicate a host name.");
		errors.append(error);
	}
	if (ui->username->text().isEmpty()) {
		error = tr("You forget to indicate the user name.");
		errors.append(error);
	}
	QString dbName;
	switch (_db->whoAmI()) {
		case Database::Type::Names :
			dbName = _db->getDBname();
			break;
		case Database::Type::Postgres :
			dbName = "";
			break;
	}
	if (!_db->canConnect(dbName)) {
		{ // scope for search if no password has been suppllied
			QRegExp rgx("no password supplied");
			if (rgx.indexIn(_db->getLastError()) != -1) {
				error = tr("Connexion refused due to missing password.");
				errors.append(error);
			}
		}
		{ // scope for wrong password supplied
			QRegExp rgx("authentification par mot de passe échouée pour l'utilisateur");
			if (rgx.indexIn(_db->getLastError()) != -1) {
				error = tr("Connexion refused due to wrong password.");
				errors.append(error);
			}
		}
		{ // scope for unknown host name
			QRegExp rgx("n'a pas pu traduire le nom d'hôte");
			if (rgx.indexIn(_db->getLastError()) != -1) {
				error = tr("Connexion refused due to unknow host name.");
				errors.append(error);
			}
		}
		{ // scope for no active host address on port
			QRegExp rgx("Le serveur est-il actif sur l'hôte");
			if (rgx.indexIn(_db->getLastError()) != -1) {
				error = tr("Connexion refused due to non active host adress/port.");
				errors.append(error);
			}
		}
	}
	return errors.isEmpty();
}

void DialogConnectServer::printWarnings() {
	QMessageBox warning;
	QString message;
	for (QString entry : errors)
		message += entry +"\n";
	int result = warning.warning(this, tr("You should give minimal informations"), message );
	if (result) qDebug() << "try again";
}
