#ifndef DIALOGNEWDATA_H
#define DIALOGNEWDATA_H

#include <QDialog>
#include <QMetaEnum>
#include <QMetaType>
#include <QComboBox>
#include <QPushButton>
#include <QLineEdit>
#include "Database/NamesDB.h"

namespace Ui { class DialogNewData; }

class DialogNewData : public QDialog {
	Q_OBJECT

public:
	explicit DialogNewData(QWidget *parent = 0);
	~DialogNewData();
	QString			getName();
	QString			getCode();
	Database::Media	getType();

private slots:
	void on_Cancel_clicked();
	void on_OK_clicked();
	void on_name_textChanged(const QString &text);
	void on_code_textChanged(const QString &text);
	void on_typeList_currentIndexChanged(const QString &text);

private:
	Ui::DialogNewData	*ui;
	QString				_name, _code;
	Database::Media		_type;
	void populateTypeList();
};

#endif // DIALOGNEWDATA_H
