#ifndef DIALOGCONNECTSERVER_H
#define DIALOGCONNECTSERVER_H

#include <QDialog>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QSpinBox>
#include "Database/db.h"

namespace Ui { class DialogConnectServer; }

class DialogConnectServer : public QDialog {
	Q_OBJECT

public:
	explicit DialogConnectServer(Database::DB *db, QWidget *parent = 0, const QString &host = QString(""));
	~DialogConnectServer();

private slots:
	void on_Cancel_clicked();
	void on_OK_clicked();
	void on_connect_clicked();
	void on_serverConnection();

signals:
	void ServerConnection();

private:
	Ui::DialogConnectServer		*ui;
	Database::DB				*_db;
	QStringList					errors;
	QString						_host;
	void populateFields();
	bool checkValidity();
	void printWarnings();
};

#endif // DIALOGCONNECTSERVER_H
