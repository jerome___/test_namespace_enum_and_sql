#include "dialogCreateDatabase.h"
#include "ui_dialogCreateDatabase.h"

DialogCreateDatabase::DialogCreateDatabase(Database::PostgresDB *d,
                                           Database::NamesDB *n,
                                           QWidget *parent) :
    QDialog(parent), postgres(d), names(n), ui(new Ui::DialogCreateDatabase) {
	ui->setupUi(this);
	connect(ui->dbName, SIGNAL(textChanged(QString)),
	        postgres, SLOT(on_defineMyDatabaseName(QString)));
}

DialogCreateDatabase::~DialogCreateDatabase() {
	delete ui;
}

void DialogCreateDatabase::on_Cancel_clicked() {
	reject();
}

void DialogCreateDatabase::on_OK_clicked() {
	postgres->setMyDatabaseComment(ui->description->toPlainText());
	if (!postgres->getMyDBname().isEmpty()) {
		postgres->createDB();
		accept();
	} else {
		QMessageBox msg;
		int result = QMessageBox::warning(this, tr("Vous oubliez quelque chose"),
		                                  tr("Indiquez le nom de la base de donnée à créer."));
		msg.exec();
	}
}
