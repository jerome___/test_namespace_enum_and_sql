#include "dialogNewData.h"
#include "ui_dialogNewData.h"

DialogNewData::DialogNewData(QWidget *parent) :
    QDialog(parent), ui(new Ui::DialogNewData) {
	ui->setupUi(this);
	populateTypeList();
}

DialogNewData::~DialogNewData() {
	delete ui;
}

QString	DialogNewData::getName() {
	return _name;
}

QString	DialogNewData::getCode() {
	return _code;
}

Database::Media DialogNewData::getType() {
	return _type;
}

void DialogNewData::on_Cancel_clicked() {
	reject();
}

void DialogNewData::on_OK_clicked() {
	accept();
}
void DialogNewData::on_name_textChanged(const QString &text) {
	_name = text;
}

void DialogNewData::on_code_textChanged(const QString &text) {
	_code = text;
}

void DialogNewData::on_typeList_currentIndexChanged(const QString &text) {
	QMetaEnum EnumMedia = QMetaEnum::fromType<Database::Media>();
	int value = EnumMedia.keyToValue(text.toLatin1());
	_type = static_cast<Database::Media>(value);
}

void DialogNewData::populateTypeList() {
	ui->typeList->clear();
	QMetaEnum EnumMedia = QMetaEnum::fromType<Database::Media>();
	for (int i(0); i < EnumMedia.keyCount(); i++) {
		QString key = EnumMedia.key(i);
		ui->typeList->addItem(key);
	}
}
