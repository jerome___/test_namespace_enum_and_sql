#include "dialogConnectDatabase.h"
#include "ui_dialogConnectDatabase.h"
#include <QMessageBox>

DialogConnectDatabase::DialogConnectDatabase(Database::PostgresDB *s,
                                             Database::NamesDB *n,
                                             QWidget *parent) :
    QDialog(parent), server(s), names(n), ui(new Ui::DialogConnectDatabase) {
	ui->setupUi(this);
	ui->deleteDB->setEnabled(false);
	connectNamesServer = new DialogConnectServer(names, this);
	databasesModel = new QStandardItemModel;
	creator = new DialogCreateDatabase(server, names, this);
	populateDatabases();
	connect(this, SIGNAL(DatabaseConnectionStateEvent()),
	        this, SLOT(on_databaseCheckConnection()));
	connect(connectNamesServer, &QDialog::finished,
	        [this](int result) {
		if (result) server->setMyOwner(names->getUser());
		emit DatabaseConnectionStateEvent();
		rejected = result; });
	connect(creator, &QDialog::finished,
	        [this](int result){	if (result) { populateDatabases(); } });
	ui->connect->setEnabled(false);
	rejected = 1;
}

DialogConnectDatabase::~DialogConnectDatabase() {
	delete databasesModel;
	delete connectNamesServer;
	delete creator;
	delete ui;
}

void DialogConnectDatabase::on_connect_clicked() {
	if (!selectedDBname.isEmpty()) {
		while (!names->canConnect(selectedDBname) && rejected != QDialog::Rejected)
			connectNamesServer->exec();
		if (rejected == QDialog::Rejected) ui->connect->setChecked(false);
		else names->setDatabaseName(selectedDBname);
		(ui->connect->isChecked()) ? names->connectDB() : names->disConnectDB();
	}
	rejected = 1;
	emit DatabaseConnectionStateEvent();
}

void DialogConnectDatabase::on_changeUser_clicked() {
	connectNamesServer->open();
}

void DialogConnectDatabase::on_createDB_clicked() {
	while (server->getMyOwner().isEmpty() && rejected != QDialog::Rejected) {
		QMessageBox artung;
		artung.setText(tr("Define a user to be able to connect."));
		artung.exec();
		connectNamesServer->exec();
	}
	if (rejected != QDialog::Rejected) creator->open();
	rejected = 1;
	emit DatabaseConnectionStateEvent();
}

void DialogConnectDatabase::on_deleteDB_clicked() {
	if (!selectedDBname.isEmpty()) {
		names->disConnectDB();
		if (server->hasTableDBName("numbers", selectedDBname)) {
			QMessageBox can;
			int answer = can.question(this,tr("Remove database"),
			                          tr("Are you sure to remove this database ?"));
			if (answer == QMessageBox::Yes ) {
				server->setMyDBname(selectedDBname);
				server->deleteDB();
				populateDatabases();
				selectedDBname = "";
			}
		} else {
			QMessageBox canNot;
			int ok = canNot.warning(this, tr("Impossible"),
			                        tr("You can not delete this database,\n"
			                           "because she has not a usable table \"numbers\""));
		}
	}
	emit DatabaseConnectionStateEvent();
}

void DialogConnectDatabase::on_Close_clicked() {
	accept();
}

void DialogConnectDatabase::on_serverConnected() {
	(server->isConnected()) ? populateDatabases() : databasesModel->clear();
}

void DialogConnectDatabase::on_databaseCheckConnection() {
	ui->connect->setChecked(names->isConnected());
	(names->isConnected()) ? ui->connect->setText(tr("Disconnect"))
	                       : ui->connect->setText(tr("Connect"));
	ui->deleteDB->setEnabled(!selectedDBname.isEmpty());
	ui->connect->setEnabled(!selectedDBname.isEmpty());
}

void DialogConnectDatabase::on_newDBcreated() {
	populateDatabases();
}

void DialogConnectDatabase::on_databases_clicked() {
	names->disConnectDB();
	QModelIndex selected = ui->databases->currentIndex();
	QModelIndex selectedName = selected.sibling(selected.row(), 0);
	selectedDBname = selectedName.data(Qt::DisplayRole).toString();
	emit DatabaseConnectionStateEvent();
}

void DialogConnectDatabase::populateDatabases() {
	databasesModel->clear();
	QMap<QString, QString> databases = server->getDatabasesList();
	qDebug() << "OK, there is " << databases.count() << " rows inside the table of this database.";
	if (!databases.isEmpty()) {
		for (QString entry : databases.keys()) {
			QList<QStandardItem*> row;
			row.append( new QStandardItem( entry ) );
			row.append( new QStandardItem( databases.value(entry) ) );
			databasesModel->appendRow(row);
		}
	}
	ui->databases->setModel(databasesModel);
	ui->databases->show();
	ui->databases->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui->databases->setSelectionMode(QAbstractItemView::SingleSelection);
	ui->databases->setEditTriggers(QAbstractItemView::NoEditTriggers);
}
