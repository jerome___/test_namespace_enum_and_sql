#ifndef POSTGRESDB_H
#define POSTGRESDB_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include "db.h"

namespace Database {
	class PostgresDB : public DB {
		Q_OBJECT

	public:
		explicit PostgresDB(QObject *parent = nullptr);
		~PostgresDB();
		Type		whoAmI() override;
		QMap<QString, QString>	getDatabasesList();
		void					createDB();
		void					deleteDB();
		QString					getMyOwner() const;
		void					setMyOwner(const QString &o);
		QString					getMyDBname() const;
		void					setMyDBname(const QString &name);
		void					setMyDatabaseComment(const QString &c);
		bool					hasTableDBName(const QString &table, const QString &dbName);

	public slots:
		void on_defineMyDatabaseName(const QString &n);

	protected:
		QMap<QString, QString>	_databases;
		QString					myDatabaseName, myOwner, myDBcomment;
		void findDatabasesList();
	};
}

#endif // POSTGRESDB_H
