#include "db.h"

Database::DB::DB(QObject *parent) : QObject(parent) {
	_dbName = "___";
}

Database::DB::~DB() { }

bool Database::DB::isConnected() { return _isConnected; }

void Database::DB::connectDB(const QString &dbName, const QString &username) {
	db.setHostName(_host);
	db.setPort(_port);
	(dbName.isEmpty()) ? db.setDatabaseName(_dbName) : db.setDatabaseName(dbName);
	_isConnected = (username.isEmpty()) ? db.open(_user, _pwd) : db.open(username, _pwd);
	if (_isConnected) {
		bool ok;
		QString str = "set application_name = test_enum;";
		ok = db.exec(str).isActive();
		if (!_isConnected || !ok)
			qDebug() << "Error SQL: " << db.lastError().text();
	} else {
		_lastError = db.lastError().text();
		qDebug() << "Can not open: " << _lastError;
	}
}

void Database::DB::disConnectDB() {
	db.close();
	_isConnected = false;
}

bool Database::DB::canConnect(const QString &name) {
	connectDB(name);
	bool ok = db.isOpen();
	disConnectDB();
	return ok;
}
QString Database::DB::getConnectionName() { return db.connectionName(); }

QString	Database::DB::getHostname() const { return _host; }

QString	Database::DB::getUsername() const {	return _user; }

QString	Database::DB::getPassword() const {	return _pwd; }

QString	Database::DB::getDBname() const { return _dbName; }

int	Database::DB::getPort() const {	return _port; }

QString	Database::DB::getLastError() { return _lastError; }

void Database::DB::on_host(const QString &s) { _host = s; }

void Database::DB::on_dbName(const QString &s) { _dbName = s; }

void Database::DB::on_user(const QString &s) { _user = s; }

void Database::DB::on_password(const QString &s) { _pwd = s; }

void Database::DB::on_port(const int &i) { _port = i; }
