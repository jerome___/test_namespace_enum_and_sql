#ifndef DB_H
#define DB_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>

namespace Database {

	enum class Type : int { Postgres, Names };

	class DB : public QObject {
		Q_OBJECT
	public:
		explicit DB(QObject *parent = nullptr);
		virtual ~DB();
		virtual Type whoAmI() =0;
		void	connectDB(const QString &dbName = QString(""),
		                  const QString &username = QString(""));
		void	disConnectDB();
		bool	canConnect(const QString &name);
		bool	isConnected();
		QString getConnectionName();
		QString	getHostname() const;
		QString	getUsername() const;
		QString	getPassword() const;
		QString	getDBname() const;
		int		getPort() const;
		QString	getLastError();

	public slots:
		void on_host(const QString &s);
		void on_dbName(const QString &s);
		void on_user(const QString &s);
		void on_port(const int &i);
		void on_password(const QString &s);

	protected:
		QSqlDatabase	db;
		QString			_host, _dbName, _user, _pwd, _lastError;
		int				_port;
		bool			_isConnected;
	};
}

#endif // DB_H
