#ifndef NAMESDB_H
#define NAMESDB_H
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include <QMap>
#include <QMetaEnum>
#include "db.h"

namespace Database {
	Q_NAMESPACE

	enum class Media : int { EMail, Skype, WhatsApp, Line,
		                     VOIPfixPhone, VOIPsoftPhone, Cellular, Fix };
	Q_ENUM_NS(Media);

	class NamesDB : public DB {
		Q_OBJECT

	public:
		explicit NamesDB(QObject *parent = nullptr);
		~NamesDB();
		Type		whoAmI() override;
		void		add(const QString &n, Media t, const QString &d);
		void		remove(const QString &n);
		void		setDatabaseName(const QString &text);
		void		setHostName(const QString &name);
		QString		getDatabaseName() const;
		QString		getUser() const;
		bool		updateData(const QString &name, Media type, const QString &code);
		bool		updateData(const QString &oldName,const QString &newName,
		                       Media type, const QString &code);
		bool		hasTable(const QString &name);
		QMap<QString, QPair<Media, QString>>	getList();

	public slots:
		void on_addData(const QString &n, Database::Media t, const QString &c);

	protected:
		QMap<QString, QPair<Media, QString>>	list; // QString is for "name" field,
		                              // Media is for "type" enum field database content


	private:
		bool loadDB();
		bool updateDB();
		bool updateDBfor(const QString &name);
		bool insertDB(const QString &n, Media t, const QString &c);
		bool removeDB(const QString &n);
	};
}
#endif // NAMESDB_H
