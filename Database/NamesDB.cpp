#include "NamesDB.h"

Database::NamesDB::NamesDB(QObject *parent) : DB(parent) {
	db = QSqlDatabase::addDatabase("QPSQL", "NamesDB_Database");
	_port = 5432;
	_isConnected = false;
}

Database::NamesDB::~NamesDB() { }

Database::Type Database::NamesDB::whoAmI() {
	return Type::Names;
}

void Database::NamesDB::add(const QString &n, Media t, const QString &c) {
	if (insertDB(n, t, c)) {
		QPair<Media, QString> value(t, c);
		list.insert(n, value);
	}
}

void Database::NamesDB::remove(const QString &n) {
	if (removeDB(n))
		list.remove(n);
}

void Database::NamesDB::setDatabaseName(const QString &text) {
	_dbName = text;
}

void Database::NamesDB::setHostName(const QString &name) {
	_host = name;
}

QString Database::NamesDB::getDatabaseName() const {
	return _dbName;
}

QMap<QString, QPair<Database::Media, QString>> Database::NamesDB::getList() {
	if (loadDB())
		return list;
	else return QMap<QString, QPair<Database::Media, QString>>();
}

QString Database::NamesDB::getUser() const {
	return _user;
}

bool Database::NamesDB::updateData(const QString &name, Media type, const QString &code) {
	if (list.contains(name)) {
		QPair<Media, QString> values(type, code);
		list[name] = values;
		return updateDBfor(name);
	}
	return false;
}

bool Database::NamesDB::updateData(const QString &oldName,
                                 const QString &newName,
                                 Media type, const QString &code) {
	bool oldRemoved, newInserted;
	if (list.contains(oldName)) {
		list.remove(oldName);
		QPair<Media, QString> values(type, code);
		list[newName] = values;
		oldRemoved = removeDB(oldName);
		newInserted = insertDB(newName, type, code);
	}
	return (!list.contains(oldName), list.contains(newName) && oldRemoved && newInserted);
}

bool Database::NamesDB::hasTable(const QString &name) {
	QString str_query = QString("SELECT exists "
	                            "( SELECT 1 from pg_catalog.pg_class c "
	                            "  JOIN pg_catalog.pg_namespace n on n.oid = c.relnamespace "
	                            "  WHERE n.nspname = 'public' "
	                            "  AND c.relname = 'numbers' "
	                            "  AND c.relkind = 'r' );");
	QSqlQuery query(db);
	bool ok(false);
	if (query.exec(str_query)) {
		while (query.next())
			ok = query.value("exists").toBool();
	} else {
		qDebug() << "SQL Error (NamesDB::hasTable can not check if table 'numbers' exist) : "
		         << query.lastError().text();
		return false;
	}
	return ok;
}

void Database::NamesDB::on_addData(const QString &n, Database::Media t, const QString &c) {
	add(n, t, c);
}

bool Database::NamesDB::loadDB() {
	QSqlQuery query(db);
	QString str = QString("SELECT * FROM numbers;");
	query.prepare(str);
	if (query.exec()) {
		list.clear();
		while (query.next()) {
			QString name = query.value("name").toString();
			Media type = query.value("type").value<Media>();
			QString code = query.value("code").toString();
			QPair<Media, QString> value(type, code);
			list.insert( name, value );
		}
	} else {
		qDebug() << "Error SQL (NamesDB::loadDB can not select) : "
		         << query.lastError().text();
		return false;
	}
	return true;
}

bool Database::NamesDB::updateDBfor(const QString &name) {
	QSqlQuery query(db);
	QString str = QString("UPDATE numbers SET "
	                      "type = :type, code = :code "
	                      "WHERE name = :name ;");
	query.prepare(str);
	QMetaEnum EnumMedia = QMetaEnum::fromType<Database::Media>();
	QString type = EnumMedia.valueToKey(static_cast<int>(list.value(name).first));
	QString code = list.value(name).second;
	query.bindValue(":name", name);
	query.bindValue(":type", type);
	query.bindValue(":code", code);
	if (!query.exec()) {
		qDebug() << "SQL Error (NamesDB::updateDBfor can not update) : "
		         << query.lastError().text();
		return false;
	}
	return true;
}

bool Database::NamesDB::updateDB() {
	QSqlQuery query(db);
	QString str = QString("UPDATE numbers SET "
	                      "type = :type, code = :code "
	                      "WHERE name = :name ;");
	query.prepare(str);
	for (QString name : list.keys()) {
		QMetaEnum EnumMedia = QMetaEnum::fromType<Database::Media>();
		QString type = EnumMedia.valueToKey(static_cast<int>(list.value(name).first));
		QString code = list.value(name).second;
		query.bindValue(":name", name);
		query.bindValue(":type", type);
		query.bindValue(":code", code);
		if (!query.exec()) {
			qDebug() << "SQL Error (NamesDB::updateDB can not update) : "
			         << query.lastError().text();
			return false;
		}
	}
	return true;
}

bool Database::NamesDB::insertDB(const QString &n, Media t, const QString &c) {
	QSqlQuery query(db);
	QString str = QString("INSERT INTO numbers "
	                      "(name, type, code) VALUES( :name, :type, :code ) ;");
	query.prepare(str);
	QMetaEnum EnumType = QMetaEnum::fromType<Database::Media>();
	QString type = EnumType.valueToKey(static_cast<int>(t));
	query.bindValue(":name", n);
	query.bindValue(":type", type);
	query.bindValue(":code", c);
	if (!query.exec()) {
		qDebug() << "SQL Error (NamesDB::insertDB can not insert) : "
		         << query.lastError().text();
		return false;
	}
	return true;
}

bool Database::NamesDB::removeDB(const QString &n) {
	QSqlQuery query(db);
	QString str = QString("DELETE FROM numbers "
	                      "WHERE name = :name ;");
	query.prepare(str);
	query.bindValue(":name", n);
	if (!query.exec()) {
		qDebug() << "SQL Error (NamesDB::removeDB can not delete) : "
		         << query.lastError().text();
		return false;
	}
	return true;
}
