#include "PostgresDB.h"

Database::PostgresDB::PostgresDB(QObject *parent) : DB(parent) {
	db = QSqlDatabase::addDatabase("QPSQL", "Administrator_Database");
	_dbName = "postgres";
	_port = 5432;
	_isConnected = false;
	findDatabasesList();
}

Database::PostgresDB::~PostgresDB() { }

Database::Type Database::PostgresDB::whoAmI() {
	return Type::Postgres;
}

void Database::PostgresDB::findDatabasesList() {
	_databases.clear();
	QString str = QString("SELECT db.datname, de.description "
	                      "FROM pg_database as db, pg_shdescription as de "
	                      "WHERE db.datistemplate = false "
	                      "AND db.oid = de.objoid ; ");
	QSqlQuery query(db);
	query.prepare(str);
	if (query.exec()) {
		while (query.next())
			_databases.insert(query.value("datname").toString(),
			                  query.value("description").toString());
	} else
		qDebug() << "Error SQL (PostgresDB::findDatabaseList) : "
		         << query.lastError().text();
}

void Database::PostgresDB::deleteDB()  {
	QSqlQuery query(db);
	QString str = QString("DROP DATABASE IF EXISTS \"%1\" ;").arg(myDatabaseName);
	if (!query.exec(str))
		qDebug() << QString("Error SQL (PostrgesqlDB::deleteDB can not delete Database \"%1\") : ")
		            .arg(myDatabaseName)
		         << db.lastError().text();
}

void Database::PostgresDB::createDB() {
	QSqlQuery query(db);
	QString str = QString("CREATE DATABASE \"%1\" with owner \"%2\";")
	              .arg(myDatabaseName).arg(myOwner);
	if (!query.exec(str))
		qDebug() << QString("SQL Error (PostgresDB::createDB can not create Database %1) : ")
		            .arg(myDatabaseName)
		         << db.lastError().text();
	else {
		if (!query.exec(QString("COMMENT ON DATABASE \"%1\" IS '%2';")
		        .arg(myDatabaseName)
		        .arg(myDBcomment)))
			qDebug() << QString("SQL Error (PostgresDB::createDB can not comment Database %1) : ")
			            .arg(myDatabaseName)
			         << db.lastError().text();
		db.addDatabase("QPSQL", myDatabaseName);
		connectDB(myDatabaseName);
	}
	str = QString("DO $$ BEGIN "
	              "IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname like '%media%') "
	              "THEN CREATE TYPE media as enum "
	              "('EMail', 'Skype', 'WhatsApp', 'Line', "
	              "'VOIPfixPhone', 'VOIPsoftPhone', 'Cellular', 'Fix'); "
	              "END IF; END$$;");;
	if (!query.exec(str))
		qDebug() << "SQL Error (PostgresDB::createDB can not create enum type \"media\") : "
		         << db.lastError().text();
	str = QString("CREATE TABLE numbers ("
	              "name varchar(24), "
	              "type media not null, "
	              "code varchar(36),"
	              "CONSTRAINT fk_numbers PRIMARY KEY(name, type, code) );");
	if (!query.exec(str))
		qDebug() << "SQL Error (Postgres::createDB Can not create table \"numbers\") : "
		         << db.lastError().text();
	else {
		str = QString("ALTER TABLE numbers OWNER TO %1").arg(myOwner);
		if (!query.exec(str))
			qDebug() << "SQL Error (Postgres::createDB can not alter table \"numbers\" for owner "
			         << myOwner << ") : " << db.lastError().text();
	}
	db = db.database("Administrator_Database");
	connectDB();
	db.removeDatabase(myDatabaseName);
}

QString Database::PostgresDB::getMyOwner() const {
	return myOwner;
}

void Database::PostgresDB::setMyOwner(const QString &o) {
	myOwner = o;
}

void Database::PostgresDB::setMyDatabaseComment(const QString &c) {
	myDBcomment = c;
}

QString Database::PostgresDB::getMyDBname() const {
	return myDatabaseName;
}

void Database::PostgresDB::setMyDBname(const QString &name) {
	myDatabaseName = name;
}

void Database::PostgresDB::on_defineMyDatabaseName(const QString &n) {
	myDatabaseName = n;
}

QMap<QString, QString> Database::PostgresDB::getDatabasesList() {
	findDatabasesList();
	return _databases;
}

bool Database::PostgresDB::hasTableDBName(const QString &table, const QString &dbName) {
	QString databaseConnection = QString("search_table_of_%1").arg(dbName);
	db.addDatabase("QPSQL", databaseConnection);
	db = db.database(databaseConnection);
	connectDB(dbName);
	QString str_query = QString("SELECT exists "
	                            "( SELECT 1 from pg_catalog.pg_class c "
	                            "  JOIN pg_catalog.pg_namespace n on n.oid = c.relnamespace "
	                            "  WHERE n.nspname = 'public' "
	                            "  AND c.relname = 'numbers' "
	                            "  AND c.relkind = 'r' );");
	QSqlQuery query(db);
	bool ok(false);
	if (query.exec(str_query)) {
		while (query.next())
			ok = query.value("exists").toBool();
	} else {
		qDebug() << "SQL Error from PostgresDB::hasTableName for check if table 'numbers' exist : "
		         << query.lastError().text();
		return false;
	}
	disConnectDB();
	db = db.database("Administrator_Database");
	db.removeDatabase(QString("search_table_of_%1").arg(dbName));
	connectDB();
	return ok;
}
