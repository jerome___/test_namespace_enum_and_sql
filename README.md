# README #
>Talk about a project test named "test_namespace_enum_and_SQL"
>
>Parle du projet "test_namespace_enum_and_SQL"

### What is this for ? (A quoi ça sert ?) ###

* Summary (résumé)
>1. It has to show how to use Qt version > Qt-5.5 Q_ENUM usage inside a C++ namsepace container.
>Il fait la démonstration de l'utilisation de la macro Qt (version > Qt-5.5) Q_ENUM dans un namespace C++.
>
>2. It shows also how to get these enum class (C++ 11 and up) for bind them in a Postgresql enum type field and read them back.
>Il montre aussi comment utiliser les objects de type enum class C++ 11 et plus dans un champ enum de base de donnée Postgresql et de les lire.
>
>3. It shows how to populate QComboBox from enum class, and how to get selection for QComboBox to enum type data.
>Il montre comment peupler un object QComboBox depuis des enum class, et comment en extraire la sélection dans un objet enum class.
>
>4. At the end time, it shows some administration of databse Postgresql and why to use deprecated QSqlDatabse methods for make it running well (QPSQL driver is very old and seems to be no more maintened).
>Et puis il montre auss comment pouvoir administrer une base de donnée Postgresql et pourquoi/comment utiliser des méthodes dépréciées de Qt-5 pour faire fonctionner des requêtes simples qui ne fonctionneriaent pas autyrement du fait de l'ancièneté du driver QPSQL (qui semble ne plus être emaintenu).
>
>5. It has translation for English and French language (feel free to add translation files for other languages)
>L'application est traduite en Français et en Anglais. (vous êtes libre de fournir plus de fichiers traductions dans différentes langues)


### Requiered (pré-requis) ###

* Postgresql server running and accessible
* Qt-5 (version > 5.5)
* C++ compiler (i use C++17 call from .pro file, but C++11 should compil)

