#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QStatusBar>
#include <QMenu>
#include <QLayout>
#include <QMenuBar>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
	ui->setupUi(this);
	_postgresql = new Database::PostgresDB(this);
	_Names = new Database::NamesDB(this);
	tableModel = new QStandardItemModel;
	itemTypeDelegate = new TypeEditComboBox(ui->table);
	ui->table->setItemDelegate(itemTypeDelegate);
	statusW = new QWidget;
	hStatusBox = new QHBoxLayout;
	statusDB = new QLabel;
	statusServer = new QLabel;
	separator = new QLabel(" --- ");
	statusDB->setText(tr("No database connected"));
	statusServer->setText(tr("No active connection"));
	hStatusBox->addWidget(statusServer);
	hStatusBox->addWidget(separator);
	hStatusBox->addWidget(statusDB);
	statusW->setLayout(hStatusBox);
	ui->statusBar->addPermanentWidget(statusW);
	ui->removeData->setEnabled(false);
	defStatusBar();
	serverPostgresConnector = new DialogConnectServer(_postgresql, this);
	serverNamesConnector = new DialogConnectServer(_Names, this);
	dbConnector = new DialogConnectDatabase(_postgresql, _Names, this);
	connect(serverPostgresConnector, SIGNAL(ServerConnection()),
	        dbConnector, SLOT(on_serverConnected()));
	connect(serverPostgresConnector, SIGNAL(ServerConnection()),
	        this, SLOT(on_serverConnection()));
	connect(serverPostgresConnector, &QDialog::finished,
	        [this](int result) {
		if (result) { }
	});
	connect(serverNamesConnector, &QDialog::finished,
	        [this](int result) {
		if (result) {
			_postgresql->setMyOwner(_Names->getUser());
		}
	});
	connect(dbConnector, SIGNAL(DatabaseConnected()),
	        this, SLOT(on_databaseConnection()));
	connect(dbConnector, &QDialog::finished,
	        [this] (int result) { if (result) on_databaseConnection(); });
	connect(this, SIGNAL(addData(QString,Database::Media, QString)),
	        _Names, SLOT(on_addData(QString,Database::Media, QString)));
	connect(this, SIGNAL(newDatabaseCreated()), dbConnector, SLOT(on_newDBcreated()));
	connect(tableModel, SIGNAL(itemChanged(QStandardItem*)),
	        this, SLOT(on_itemTableChanged(QStandardItem*)));
	ui->table->hide();
	colorMessage.setColor(QPalette::WindowText, Qt::blue);
	noConnectionDatabase = tr("You need to connect on a database \n"
	                          "who has a usable table.");
	ui->information->setPalette(colorMessage);
	ui->information->setText(noConnectionDatabase);
}

MainWindow::~MainWindow() {
	delete tableModel;
	delete statusDB;
	delete statusServer;
	delete separator;
	delete hStatusBox;
	delete statusW;
	delete itemTypeDelegate;
	if (_Names != nullptr)
		delete _Names;
	if (_postgresql != nullptr)
		delete _postgresql;
	delete serverPostgresConnector;
	delete serverNamesConnector;
	delete dbConnector;
	delete ui;
}

void MainWindow::on_serverConnection() {
	defStatusBar();
}

void MainWindow::on_databaseConnection() {
	defStatusBar();
	if (_Names->isConnected()) {
		if (checkIfContainTable()) {
			populateTable();
			ui->information->hide();
			colorMessage.setColor(QPalette::WindowText, Qt::green);
			ui->information->setText(tr("Ok, this databse has a usable table."));
		} else {
			ui->table->hide();
			tableModel->clear();
			ui->information->show();
			colorMessage.setColor(QPalette::WindowText, Qt::red);
			ui->information->setText(tr("For this database, there is no correct table.\n"
			                            "You need to choose an other one one database\n"
			                            "or create a new one."));
		}
	} else {
		tableModel->clear();
		ui->table->hide();
		ui->information->show();
		colorMessage.setColor(QPalette::WindowText, Qt::blue);
		ui->information->setText(noConnectionDatabase);
	}
	ui->information->setPalette(colorMessage);
}

void MainWindow::on_actionConnect_Server_triggered() {
	serverPostgresConnector->open();
}

void MainWindow::on_actionConnect_Database_triggered() {
	dbConnector->exec();
}

void MainWindow::on_newData_clicked() {
	DialogNewData *NewData = new DialogNewData(this);
	connect(NewData, &QDialog::finished,
	        [NewData, this] (int result) {
		if (result) {
			if (_Names->isConnected())
				_Names->add(NewData->getName(), NewData->getType(), NewData->getCode());
			populateTable();
		}
		NewData->deleteLater();
	});
	NewData->open();
}

void MainWindow::on_removeData_clicked() {
	_Names->remove(selectedName);
	populateTable();
}

void MainWindow::on_actionQuit_triggered() {
	if (_postgresql->isConnected())
		_postgresql->disConnectDB();
	if (_Names->isConnected())
		_Names->disConnectDB();
	this->close();
}

void MainWindow::on_table_clicked() {
	QModelIndex selected = ui->table->currentIndex();
	QModelIndex toActOn = selected.sibling(selected.row(), 0);
	selectedName = toActOn.data(Qt::DisplayRole).toString();
	ui->removeData->setEnabled(!selectedName.isEmpty());
}

void MainWindow::populateTable() {
	tableModel->clear();
	QStringList headers;
	headers.append(tr("Nom"));
	headers.append(tr("Type"));
	headers.append(tr("Code"));
	ui->table->horizontalHeader()->show();
	ui->table->verticalHeader()->hide();
	tableModel->setHorizontalHeaderLabels(headers);
	QMap<QString, QPair<Database::Media, QString>> list = _Names->getList();
	if (!list.isEmpty()) {
		QMetaEnum EnumMedia = QMetaEnum::fromType<Database::Media>();
		for (QString name: list.keys()) {
			QList<QStandardItem*> row;
			QString type = EnumMedia.valueToKey(static_cast<int>(list.value(name).first));
			QString code = list.value(name).second;
			row.append(new QStandardItem(name));
			row.append(new QStandardItem(type));
			row.append(new QStandardItem(code));
			tableModel->appendRow(row);
		}
	}
	ui->table->setModel(tableModel);
	ui->table->show();
}

void MainWindow::on_itemTableChanged(QStandardItem *item) {
	int rowItem = item->row();
	QModelIndex indexItemName = tableModel->index(rowItem, 0, QModelIndex());
	QModelIndex indexItemType = tableModel->index(rowItem, 1, QModelIndex());
	QModelIndex indexItemCode = tableModel->index(rowItem, 2, QModelIndex());
	QString name = indexItemName.data(Qt::DisplayRole).toString();
	Database::Media type = indexItemType.data(Qt::DisplayRole).value<Database::Media>();
	QString code = indexItemCode.data(Qt::DisplayRole).toString();
	bool ok = (item->column() != 0) ? _Names->updateData(name, type, code)
	                                : _Names->updateData(selectedName, name, type, code);
	if (ok) populateTable() ;
	else qDebug() << "FAILED TO UPDATE DATAS !";
}

void MainWindow::defStatusBar() {
	QPalette colorServer, colorDB;
	if (_postgresql->isConnected()) {
		statusServer->setText(tr("Adiminstrator connected to server "));
		colorServer.setColor(QPalette::WindowText, Qt::green);
	} else {
		statusServer->setText(tr("No administrator connection "));
		colorServer.setColor(QPalette::WindowText, Qt::red);
	}
	if (_Names->isConnected()) {
		statusDB->setText(tr("Connected to databse %1").arg(_Names->getDatabaseName()));
		colorDB.setColor(QPalette::WindowText, Qt::green);
	} else {
		statusDB->setText(tr("No connection to database."));
		colorDB.setColor(QPalette::WindowText, Qt::red);
	}
	statusServer->setPalette(colorServer);
	statusDB->setPalette(colorDB);
	ui->actionConnect_Database->setEnabled(_postgresql->isConnected());
	ui->actionCreate_Database->setEnabled(_postgresql->isConnected());
	ui->newData->setEnabled(_Names->isConnected());
}

bool MainWindow::checkIfContainTable() {
	return _Names->hasTable("numbers");
}
