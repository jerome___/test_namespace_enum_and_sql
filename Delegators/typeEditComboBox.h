#ifndef TYPEEDITCOMBOBOX_H
#define TYPEEDITCOMBOBOX_H

#include <QWidget>
#include <QStyledItemDelegate>

class TypeEditComboBox : public QStyledItemDelegate {
public:
	TypeEditComboBox(QObject *parent = 0);
	~TypeEditComboBox();
	virtual QWidget *createEditor(QWidget *parent,
	                              const QStyleOptionViewItem &option,
	                              const QModelIndex &index) const override;
	virtual void	setEditorData(QWidget *editor, const QModelIndex &index) const override;
	virtual void	setModelData(QWidget *editor, QAbstractItemModel *model,
	                             const QModelIndex &index) const override;

};

#endif // TYPEEDITCOMBOBOX_H
