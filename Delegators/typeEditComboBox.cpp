#include "typeEditComboBox.h"
#include <QComboBox>
#include <QMetaEnum>
#include "Database/NamesDB.h"

TypeEditComboBox::TypeEditComboBox(QObject *parent) : QStyledItemDelegate(parent) {

}

TypeEditComboBox::~TypeEditComboBox() { }

QWidget* TypeEditComboBox::createEditor(QWidget *parent,
                                        const QStyleOptionViewItem &option,
                                        const QModelIndex &index) const {
	if (index.column() != 1) // comboBox will only be implemented on edit for column "Type" (2)
		return QStyledItemDelegate::createEditor(parent, option, index);
	QComboBox* cb = new QComboBox(parent);
	QMetaEnum EnumMedia = QMetaEnum::fromType<Database::Media>();
	for (int i(0); i < EnumMedia.keyCount(); i++) {
		QString key = EnumMedia.key(i);
		cb->addItem(key);
	}
	return cb;
}

void TypeEditComboBox::setEditorData(QWidget *editor, const QModelIndex &index) const {
	if (QComboBox* cb = qobject_cast<QComboBox*>(editor)) {
		   QString currentText = index.data(Qt::EditRole).toString();
		   int cbIndex = cb->findText(currentText);
		   if (cbIndex >= 0)
			   cb->setCurrentIndex(cbIndex);
	    } else {
		    QStyledItemDelegate::setEditorData(editor, index);
	    }
}

void TypeEditComboBox::setModelData(QWidget *editor, QAbstractItemModel *model,
                                    const QModelIndex &index) const {
	if (QComboBox* cb = qobject_cast<QComboBox*>(editor))
		    model->setData(index, cb->currentText(), Qt::EditRole);
	    else
		    QStyledItemDelegate::setModelData(editor, model, index);
}
