#-------------------------------------------------
#
# Project created by QtCreator 2017-11-25T07:50:46
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = test_namespace_enum_and_SQL
TEMPLATE = app
CONFIG += C++17
QMAKE_CXXFLAGS += -std=c++1z

DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# And select to disable deprecated APIs only up to a certain version of Qt.
# example above disables all the APIs deprecated before Qt 6.0.0
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000


SOURCES += \
    main.cpp \
    mainwindow.cpp \
    Delegators/typeEditComboBox.cpp \
    Dialog/dialogConnectDatabase.cpp \
    Dialog/dialogConnectServer.cpp \
    Dialog/dialogCreateDatabase.cpp \
    Dialog/dialogNewData.cpp \
    Database/db.cpp \
    Database/PostgresDB.cpp \
    Database/NamesDB.cpp

HEADERS += \
    mainwindow.h \
    Delegators/typeEditComboBox.h \
    Dialog/dialogConnectDatabase.h \
    Dialog/dialogConnectServer.h \
    Dialog/dialogCreateDatabase.h \
    Dialog/dialogNewData.h \
    Database/db.h \
    Database/PostgresDB.h \
    Database/NamesDB.h

FORMS += \
    mainwindow.ui \
    Dialog/dialogCreateDatabase.ui \
    Dialog/dialogConnectDatabase.ui \
    Dialog/dialogConnectServer.ui \
    Dialog/dialogNewData.ui

TRANSLATIONS += \
    Traductions/fr.ts \
    Traductions/en.ts

RESOURCES += \
    Traductions/resources.qrc
